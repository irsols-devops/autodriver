Building configuration...

Current configuration : 1549 bytes
!
! Last configuration change at 17:33:46 UTC Tue May 1 2018 by cisco
!
version 16.6
service timestamps debug datetime msec
service timestamps log datetime msec
platform qfp utilization monitor load 80
no platform punt-keepalive disable-kernel-core
platform console virtual
!
hostname csr1kv-22
!
boot-start-marker
boot-end-marker
!
!
enable password ********
!
no aaa new-model
!
!
!
!
!
!
!
ip domain name irsols.com
!
!
!
!
!
!
!
!
!
!
subscriber templating
! 
! 
! 
! 
!
!
!
multilink bundle-name authenticated
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
license udi pid CSR1000V sn 9DLFR3CC3TQ
diagnostic bootup level minimal
file prompt quiet
spanning-tree extend system-id
!
!
!
username cisco password 0 ********
!
redundancy
!
!
!
!
!
lldp run
!
cdp run
! 
!
!
!
!
!
!
!
!
!
!
!
!
! 
! 
!
!
interface GigabitEthernet4
 ip address 10.10.1.22 255.255.255.0
 negotiation auto
 no mop enabled
 no mop sysid
!
interface GigabitEthernet5
 no ip address
 shutdown
 negotiation auto
 no mop enabled
 no mop sysid
!
interface GigabitEthernet6
 no ip address
 shutdown
 negotiation auto
 no mop enabled
 no mop sysid
!
!
virtual-service csr_mgmt
!
ip forward-protocol nd
no ip http server
no ip http secure-server
!
!
!
!
!
!
control-plane
!
!
!
!
!
!
line con 0
 stopbits 1
line vty 0
 exec-timeout 0 0
 login local
 transport input ssh
line vty 1
 exec-timeout 0 0
 login local
 length 0
 transport input ssh
line vty 2 4
 exec-timeout 0 0
 login local
 transport input ssh
!
wsma agent exec
!
wsma agent config
!
wsma agent filesys
!
wsma agent notify
!
!
end